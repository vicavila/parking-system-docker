package com.parking.system.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.parking.system.model.ParkedVehicleModel;

@Repository
public interface ParkedVehiclesRepository extends JpaRepository<ParkedVehicleModel, Long>{

	@Query(value = "SELECT * FROM parked_vehicle pv "
			+ "JOIN parking_lot pl on pv.parking_lot_id = pl.parking_lot_id "
			+ "WHERE pl.parking_lot_is_available = false "
			+ "AND pv.parked_vehicle_park_in is not null "
			+ "AND pv.parked_vehicle_park_out is null ", nativeQuery =true)
	List<ParkedVehicleModel> getParkedVehicles();

	@Query(value = "SELECT EXISTS ( "
			+ "SELECT true FROM parked_vehicle pv "
			+ "JOIN vehicle v on pv.vehicle_id = v.vehicle_id "
			+ "WHERE v.vehicle_number = ?1 AND pv.parked_vehicle_park_out is null);", nativeQuery =true)
	boolean isVehicleAlreadyParked(String vehicleNumber);

	@Query(value = "SELECT EXISTS ( "
			+ "SELECT parked_vehicle_id FROM parked_vehicle pv "
			+ "JOIN vehicle v on pv.vehicle_id = v.vehicle_id "
			+ "WHERE v.vehicle_number = ?1 "
			+ "AND pv.parked_vehicle_park_in is null);", nativeQuery =true)
	boolean isVehicleNotParked(String vehicleNumber);

	@Query(value = "SELECT * FROM parked_vehicle pv "
			+ "JOIN parking_lot pl on pv.parking_lot_id = pl.parking_lot_id "
			+ "JOIN vehicle v on pv.vehicle_id = v.vehicle_id "
			+ "WHERE pl.parking_lot_is_available = false "
			+ "AND v.vehicle_number = ?1 "
			+ "AND pv.parked_vehicle_park_in is not null "
			+ "AND pv.parked_vehicle_park_out is null ", nativeQuery =true)
	Optional<ParkedVehicleModel> getParkedVehicleByNumber(String vehicleNumber);

}
