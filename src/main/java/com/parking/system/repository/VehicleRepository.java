package com.parking.system.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.parking.system.model.VehicleModel;

@Repository
public interface VehicleRepository extends JpaRepository<VehicleModel, Long>{

	@Query(value = "SELECT * FROM vehicle "
			+ "WHERE vehicle_number = ?1 ", nativeQuery =true)
	Optional<VehicleModel> findByVehicleNumber(String vehicleNumber);

}
