package com.parking.system.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.parking.system.model.ParkingLotModel;

@Repository
public interface ParkingLotRepository extends JpaRepository<ParkingLotModel, Long>{

	@Query(value = "SELECT * FROM parking_lot "
			+ "WHERE parking_lot_is_available = true "
			+ "ORDER BY parking_lot_id ", nativeQuery =true)
	List<ParkingLotModel> getFreeParkingSlot();

	@Query(value = "SELECT EXISTS ( "
			+ "SELECT parking_lot_id FROM parking_lot "
			+ "WHERE parking_lot_is_available = false "
			+ "AND parking_lot_id = ?1);", nativeQuery =true)
	boolean isSlotOccupied(long parkingLotId);

}
