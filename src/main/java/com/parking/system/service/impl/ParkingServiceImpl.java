package com.parking.system.service.impl;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.parking.system.dto.ParkVehicleDTO;
import com.parking.system.dto.ParkedVehiclesDTO;
import com.parking.system.dto.ParkingLotDTO;
import com.parking.system.exception.BadRequestException;
import com.parking.system.model.ParkedVehicleModel;
import com.parking.system.model.ParkingLotModel;
import com.parking.system.model.VehicleModel;
import com.parking.system.repository.ParkedVehiclesRepository;
import com.parking.system.repository.ParkingLotRepository;
import com.parking.system.repository.VehicleRepository;
import com.parking.system.service.ParkingService;

@Service
public class ParkingServiceImpl implements ParkingService{

	// FIXED CHARGED FOR FIRST 3 HRS OR BELOW 3 HRS
	private static final double FIXED_CHARGED = 100.0; 
	
	// HOURLY FEE FOR ADDITIONAL CHARGED AFTER 3 HRS.
	// ON OR BELOW 1 HR
	private static final double HOURLY_CHARGED = 50.0; 
	
	@Autowired
	private VehicleRepository vehicleRepo;
	
	@Autowired 
	private ParkingLotRepository parkingLotRepo;
	
	@Autowired
	private ParkedVehiclesRepository parkedVehicleRepo;
	
	@Override
	public List<ParkedVehiclesDTO> getParkedVehicles() {

		List<ParkedVehicleModel> parkedVehicles = parkedVehicleRepo.getParkedVehicles();

		return parkedVehicles.stream()
				.map(pvm -> {
					return parkedModelToDto(pvm);
				}).collect(Collectors.toList());
	}

	@Override
	public List<ParkingLotDTO> getFreeParkingSlot() {

		List<ParkingLotModel> freeParkingSlot = parkingLotRepo.getFreeParkingSlot();

		return freeParkingSlot.stream()
				.map(pm -> {
						return new ParkingLotDTO(
								pm.getId(), 
								pm.getFloor(), 
								pm.getSlot(), 
								pm.isAvailable());
				}).collect(Collectors.toList());
	}

	@Override
	public ParkedVehiclesDTO getParkedVehicle(String vehicleNumber) {

		Optional<ParkedVehicleModel> pvmOptional = 
				parkedVehicleRepo.getParkedVehicleByNumber(vehicleNumber);
		
		if(parkedVehicleRepo.isVehicleNotParked(vehicleNumber) || pvmOptional.isEmpty())
			throw new BadRequestException(
					String.format("Error! Vehicle %s is not parked.", vehicleNumber));
		
		ParkedVehicleModel pvm = pvmOptional.get();
		
		return parkedModelToDto(pvm);
	}

	@Transactional
	@Override
	public void parkVehicle(ParkVehicleDTO parkVehicle) {

		if(parkVehicle == null)
			throw new BadRequestException("Error! Parameter is null.");
		
		if(isStringNull(parkVehicle.getVehicleNumber()))
			throw new BadRequestException("Error! Vehicle Number is required.");
		
		Optional<VehicleModel> vehicleOptional = 
				vehicleRepo.findByVehicleNumber(parkVehicle.getVehicleNumber());
		if(vehicleOptional.isEmpty())
			throw new BadRequestException(
					String.format("Error! Vehicle %s does not exist.", parkVehicle.getVehicleNumber()));
		
		Optional<ParkingLotModel> parkingOptional = checkCommonParkingValidation(parkVehicle);
	
		VehicleModel vehicleModel = vehicleOptional.get();
		
		// PRIVATE METHOD TO PARK VEHICLE
		parkVehicleOnFreeSlot(parkingOptional, vehicleModel);
	}

	@Transactional
	@Override
	public void parkVehicleNew(ParkVehicleDTO parkVehicle) {

		if(parkVehicle == null)
			throw new BadRequestException("Error! Parameter is null.");
		
		if(isStringNull(parkVehicle.getVehicleOwnerName()))
			throw new BadRequestException("Error! Vehicle Owner Name is required.");
		
		if(isStringNull(parkVehicle.getVehicleNumber()))
			throw new BadRequestException("Error! Vehicle Number is required.");
		
		Optional<ParkingLotModel> parkingOptional = checkCommonParkingValidation(parkVehicle);
		
		Optional<VehicleModel> vehicleOptional = vehicleRepo.findByVehicleNumber(parkVehicle.getVehicleNumber());
		
		VehicleModel vehicleModel = null;
		if(vehicleOptional.isEmpty()) {
			
			vehicleModel = new VehicleModel();
			vehicleModel.setNumber(parkVehicle.getVehicleNumber());
			vehicleModel.setOwnerName(parkVehicle.getVehicleOwnerName());
			vehicleModel.setRegisteredAt(LocalDate.now());
		}
		else
			vehicleModel = vehicleOptional.get();
			
		// PRIVATE METHOD TO PARK VEHICLE
		parkVehicleOnFreeSlot(parkingOptional, vehicleModel);
		
	}

	@Transactional
	@Override
	public ParkedVehiclesDTO parkOut(String vehicleNumber) {

		Optional<VehicleModel> vehicleOptional = vehicleRepo.findByVehicleNumber(vehicleNumber);
		if(vehicleOptional.isEmpty())
			throw new BadRequestException(
					String.format("Error! Vehicle %s does not exist.", vehicleNumber));
		
		Optional<ParkedVehicleModel> pvmOptional = 
				parkedVehicleRepo.getParkedVehicleByNumber(vehicleNumber);
		if(parkedVehicleRepo.isVehicleNotParked(vehicleNumber) || pvmOptional.isEmpty())
			throw new BadRequestException(
					String.format("Error! Vehicle %s is not parked.", vehicleNumber));
		
		ParkedVehicleModel pvModel = pvmOptional.get();
		pvModel.setParkedOut(LocalDateTime.now());
		
		double currentCharge = computeCurrentCharge(pvModel.getParkedIn());
		pvModel.setTotalCharged(currentCharge);
		
		ParkingLotModel parkingLotModel = parkingLotRepo.findById(pvModel.getParkingLot().getId()).get();
		parkingLotModel.setAvailable(true);
		
		parkingLotRepo.save(parkingLotModel);
		parkedVehicleRepo.save(pvModel);
		
		return parkedModelToDto(pvModel);
	}

	private Optional<ParkingLotModel> checkCommonParkingValidation(ParkVehicleDTO parkVehicle) {
		
		if(parkedVehicleRepo.isVehicleAlreadyParked(parkVehicle.getVehicleNumber()))
			throw new BadRequestException(
					String.format("Error! Vehicle %s is already parked.", parkVehicle.getVehicleNumber()));
		
		Optional<ParkingLotModel> parkingOptional = parkingLotRepo.findById(parkVehicle.getParkingLotId());
		if(parkingOptional.isEmpty()) 
			throw new BadRequestException(String.format("Error! Parking lot id '%s' does not exist.",
					parkVehicle.getParkingLotId()));
		
		if(!parkingOptional.get().isAvailable())
			throw new BadRequestException(String.format("Error! Parking lot id '%s' is occupied.",
					 parkVehicle.getParkingLotId()));
		
		return parkingOptional;
	}

	private ParkedVehiclesDTO parkedModelToDto(ParkedVehicleModel pvm) {
		
		ParkedVehiclesDTO pvDto = new ParkedVehiclesDTO();
		pvDto.setParkingId(pvm.getParkingLot().getId());
		pvDto.setFloor(pvm.getParkingLot().getFloor());
		pvDto.setSlot(pvm.getParkingLot().getSlot());
		pvDto.setVehicleId(pvm.getVehicle().getId());
		pvDto.setVehicleNumber(pvm.getVehicle().getNumber());
		pvDto.setVehicleOwnerName(pvm.getVehicle().getOwnerName());
		pvDto.setParkedIn(pvm.getParkedIn());
		pvDto.setParkedOut(pvm.getParkedOut());
	
		double currentCharge = computeCurrentCharge(pvDto.getParkedIn());
		pvDto.setCurrentCharge(currentCharge);
		
		return pvDto;
	}

	private double computeCurrentCharge(LocalDateTime parkedIn) {

		int minutes = (int) ChronoUnit.MINUTES.between(parkedIn, LocalDateTime.now());
		int excessHours = minutes / 60;
		excessHours += minutes % 60;
		double currentCharge = FIXED_CHARGED + (excessHours * HOURLY_CHARGED);
		
		return currentCharge;
	}

	private void parkVehicleOnFreeSlot(Optional<ParkingLotModel> parkingOptional, VehicleModel vehicleModel) {
		
		ParkingLotModel parkingLotModel = parkingOptional.get();
		parkingLotModel.setAvailable(false);
		
		ParkedVehicleModel pvm = new ParkedVehicleModel();
		pvm.setParkedIn(LocalDateTime.now());
		pvm.setVehicle(vehicleModel);
		pvm.setParkingLot(parkingLotModel);
		pvm.setTotalCharged(FIXED_CHARGED);
		
		parkingLotRepo.save(parkingLotModel);
		vehicleRepo.save(vehicleModel);
		parkedVehicleRepo.save(pvm);
	}

	private boolean isStringNull(String str) {
	
		if(str == null || str.isEmpty())
			return true;
		return false;
	}

}
