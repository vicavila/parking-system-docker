package com.parking.system.service;

import java.util.List;

import com.parking.system.dto.ParkVehicleDTO;
import com.parking.system.dto.ParkedVehiclesDTO;
import com.parking.system.dto.ParkingLotDTO;

public interface ParkingService {

	List<ParkedVehiclesDTO> getParkedVehicles();

	List<ParkingLotDTO> getFreeParkingSlot();

	ParkedVehiclesDTO getParkedVehicle(String number);

	void parkVehicle(ParkVehicleDTO parkVehicle);

	void parkVehicleNew(ParkVehicleDTO parkVehicle);

	ParkedVehiclesDTO parkOut(String vehicleNumber);

}
