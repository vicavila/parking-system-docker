package com.parking.system.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.client.HttpServerErrorException.InternalServerError;

import com.parking.system.dto.ErrorResponseDTO;

@ControllerAdvice
public class GlobalExceptionHandler {

	@ExceptionHandler(value = {BadRequestException.class})
	public ResponseEntity<Object> handleBadRequestException(BadRequestException e) {
		
		ErrorResponseDTO res = new ErrorResponseDTO(
				HttpStatus.BAD_REQUEST.value(), e.getMessage());
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(res);
	}
	
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler(value = {InternalServerError.class})
	public ResponseEntity<Object> handleInternalServerError(Exception e) {
		
		ErrorResponseDTO res = new ErrorResponseDTO(
				HttpStatus.INTERNAL_SERVER_ERROR.value(), 
				"Internal server error. Check logs for more details.");
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(res);
	}
}
