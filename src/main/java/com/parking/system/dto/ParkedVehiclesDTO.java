package com.parking.system.dto;

import java.time.LocalDateTime;

public class ParkedVehiclesDTO {

	private long parkingId;
	private String floor;
	private String slot;
	private long vehicleId;
	private String vehicleNumber;
	private String vehicleOwnerName;
	private LocalDateTime parkedIn;
	private LocalDateTime parkedOut;
	
	private double currentCharge;

	public long getParkingId() {
		return parkingId;
	}

	public void setParkingId(long parkingId) {
		this.parkingId = parkingId;
	}

	public String getFloor() {
		return floor;
	}

	public void setFloor(String floor) {
		this.floor = floor;
	}

	public String getSlot() {
		return slot;
	}

	public void setSlot(String slot) {
		this.slot = slot;
	}

	public long getVehicleId() {
		return vehicleId;
	}

	public void setVehicleId(long vehicleId) {
		this.vehicleId = vehicleId;
	}

	public String getVehicleNumber() {
		return vehicleNumber;
	}

	public void setVehicleNumber(String vehicleNumber) {
		this.vehicleNumber = vehicleNumber;
	}

	public String getVehicleOwnerName() {
		return vehicleOwnerName;
	}

	public void setVehicleOwnerName(String vehicleOwnerName) {
		this.vehicleOwnerName = vehicleOwnerName;
	}

	public LocalDateTime getParkedIn() {
		return parkedIn;
	}

	public void setParkedIn(LocalDateTime parkedIn) {
		this.parkedIn = parkedIn;
	}

	public double getCurrentCharge() {
		return currentCharge;
	}

	public void setCurrentCharge(double currentCharge) {
		this.currentCharge = currentCharge;
	}

	public LocalDateTime getParkedOut() {
		return parkedOut;
	}

	public void setParkedOut(LocalDateTime parkedOut) {
		this.parkedOut = parkedOut;
	}

}
