package com.parking.system.dto;

public class ParkVehicleDTO {

	private String vehicleNumber;
	private String vehicleOwnerName;
	private long parkingLotId;
	
	public String getVehicleNumber() {
		return vehicleNumber;
	}
	public void setVehicleNumber(String vehicleNumber) {
		this.vehicleNumber = vehicleNumber;
	}
	public String getVehicleOwnerName() {
		return vehicleOwnerName;
	}
	public void setVehicleOwnerName(String vehicleOwnerName) {
		this.vehicleOwnerName = vehicleOwnerName;
	}
	public long getParkingLotId() {
		return parkingLotId;
	}
	public void setParkingLotId(long parkingLotId) {
		this.parkingLotId = parkingLotId;
	}
	
	
}
