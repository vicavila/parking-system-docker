package com.parking.system.dto;

public class ParkingLotDTO {

	private long id;
	private String floor;
	private String slot;
	private boolean isAvailable;
	
	public ParkingLotDTO() {}
	
	public ParkingLotDTO(long id, String floor, String slot, boolean isAvailable) {
		this.id = id;
		this.floor = floor;
		this.slot = slot;
		this.isAvailable = isAvailable;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getFloor() {
		return floor;
	}
	public void setFloor(String floor) {
		this.floor = floor;
	}
	public String getSlot() {
		return slot;
	}
	public void setSlot(String slot) {
		this.slot = slot;
	}
	public boolean isAvailable() {
		return isAvailable;
	}
	public void setAvailable(boolean isAvailable) {
		this.isAvailable = isAvailable;
	}
	
	
}
