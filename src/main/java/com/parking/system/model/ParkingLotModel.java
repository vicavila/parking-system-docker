package com.parking.system.model;

import static javax.persistence.GenerationType.SEQUENCE;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity(name = "parking_lot")
@Table(name = "parking_lot")
@Embeddable
@AttributeOverrides({
		@AttributeOverride(name = "floor", column = @Column(name = "parking_lot_floor", nullable = false, length = 20)),
		@AttributeOverride(name = "slot", column = @Column(name = "parking_lot_slot", nullable = false, length = 20)),
		@AttributeOverride(name = "isAvailable", column = @Column(name = "parking_lot_is_available", columnDefinition = "boolean default true")) })
public class ParkingLotModel {

	@Id
	@SequenceGenerator(name = "parking_lot_sequence", sequenceName = "parking_lot_sequence", allocationSize = 1)
	@GeneratedValue(strategy = SEQUENCE, generator = "parking_lot_sequence")
	@Column(name = "parking_lot_id")
	private long id;

	private String floor;

	private String slot;

	private boolean isAvailable;

	@OneToMany(mappedBy = "parkingLot", cascade = CascadeType.ALL)
	private Set<ParkedVehicleModel> parkedVehicles = new HashSet<ParkedVehicleModel>();

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFloor() {
		return floor;
	}

	public void setFloor(String floor) {
		this.floor = floor;
	}

	public String getSlot() {
		return slot;
	}

	public void setSlot(String slot) {
		this.slot = slot;
	}

	public boolean isAvailable() {
		return isAvailable;
	}

	public void setAvailable(boolean isAvailable) {
		this.isAvailable = isAvailable;
	}

	public Set<ParkedVehicleModel> getParkedVehicles() {
		return parkedVehicles;
	}

	public void setParkedVehicles(Set<ParkedVehicleModel> parkedVehicles) {
		this.parkedVehicles = parkedVehicles;
	}

}
