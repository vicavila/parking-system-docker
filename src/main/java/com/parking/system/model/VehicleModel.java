package com.parking.system.model;

import static javax.persistence.GenerationType.SEQUENCE;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity(name = "vehicle")
@Table(name = "vehicle")
@Embeddable
@AttributeOverrides({ 
		@AttributeOverride(name = "number", column = @Column(name = "vehicle_number", nullable = false, length = 20)),
		@AttributeOverride(name = "registeredAt", column = @Column(name = "vehicle_registered_at", nullable = false)),
		@AttributeOverride(name = "ownerName", column = @Column(name = "vehicle_owner_name", nullable = false, length = 100)), })
public class VehicleModel {

	@Id
	@SequenceGenerator(name = "vehicle_sequence", sequenceName = "vehicle_sequence", allocationSize = 1)
	@GeneratedValue(strategy = SEQUENCE, generator = "vehicle_sequence")
	@Column(name = "vehicle_id")
	private long id;

	private String number;

	private LocalDate registeredAt;

	private String ownerName;

	@OneToMany(mappedBy = "vehicle", cascade = CascadeType.ALL)
	private Set<ParkedVehicleModel> parkedVehicles = new HashSet<ParkedVehicleModel>();
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public LocalDate getRegisteredAt() {
		return registeredAt;
	}

	public void setRegisteredAt(LocalDate registeredAt) {
		this.registeredAt = registeredAt;
	}

	public String getOwnerName() {
		return ownerName;
	}

	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}

	public Set<ParkedVehicleModel> getParkedVehicles() {
		return parkedVehicles;
	}

	public void setParkedVehicles(Set<ParkedVehicleModel> parkedVehicles) {
		this.parkedVehicles = parkedVehicles;
	}
	

}
