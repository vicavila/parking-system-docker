package com.parking.system.model;

import static javax.persistence.GenerationType.SEQUENCE;

import java.time.LocalDateTime;
import java.util.Objects;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity(name = "parked_vehicle")
@Table(name = "parked_vehicle")
@Embeddable
@AttributeOverrides({
		@AttributeOverride(name = "parkedIn", column = @Column(name = "parked_vehicle_park_in", nullable = false)),
		@AttributeOverride(name = "parkedOut", column = @Column(name = "parked_vehicle_park_out", nullable = true)),
		@AttributeOverride(name = "totalCharged", column = @Column(name = "parked_vehicle_total_charged"))})
public class ParkedVehicleModel {

	@Id
	@SequenceGenerator(name = "parked_vehicle_sequence", sequenceName = "parked_vehicle_sequence", allocationSize = 1)
	@GeneratedValue(strategy = SEQUENCE, generator = "parked_vehicle_sequence")
	@Column(name = "parked_vehicle_id")
	private long id;

	private LocalDateTime parkedIn;

	private LocalDateTime parkedOut;
	
	private double totalCharged;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "vehicle_id")
	private VehicleModel vehicle;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "parking_lot_id")
	private ParkingLotModel parkingLot;

	public LocalDateTime getParkedIn() {
		return parkedIn;
	}

	public void setParkedIn(LocalDateTime parkedIn) {
		this.parkedIn = parkedIn;
	}

	public LocalDateTime getParkedOut() {
		return parkedOut;
	}

	public void setParkedOut(LocalDateTime parkedOut) {
		this.parkedOut = parkedOut;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public VehicleModel getVehicle() {
		return vehicle;
	}

	public void setVehicle(VehicleModel vehicle) {
		this.vehicle = vehicle;
	}

	public ParkingLotModel getParkingLot() {
		return parkingLot;
	}

	public void setParkingLot(ParkingLotModel parkingLot) {
		this.parkingLot = parkingLot;
	}

	@Override
	public int hashCode() {
		return Objects.hash(vehicle.getNumber(), parkingLot.getFloor(), parkingLot.getSlot(), parkedIn);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof ParkedVehicleModel))
			return false;

		ParkedVehicleModel that = (ParkedVehicleModel) obj;
		return Objects.equals(vehicle.getNumber(), that.vehicle.getNumber())
				&& Objects.equals(vehicle.getOwnerName(), that.vehicle.getOwnerName())
				&& Objects.equals(parkingLot.getFloor(), that.parkingLot.getFloor())
				&& Objects.equals(parkingLot.getSlot(), that.parkingLot.getSlot());
	}

	public double getTotalCharged() {
		return totalCharged;
	}

	public void setTotalCharged(double totalCharged) {
		this.totalCharged = totalCharged;
	}

}
