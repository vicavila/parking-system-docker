package com.parking.system.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.parking.system.dto.ParkVehicleDTO;
import com.parking.system.dto.ParkedVehiclesDTO;
import com.parking.system.dto.ParkingLotDTO;
import com.parking.system.dto.SuccessResponseDTO;
import com.parking.system.service.ParkingService;

@RestController
@RequestMapping("/v1")
public class ParkingController {

	@Autowired
	private ParkingService parkingService;
	
	@GetMapping("/parked-vehicles")
	public ResponseEntity<Object> getListOfParkedVehicles() {
		
		List<ParkedVehiclesDTO> parkedVehicles = parkingService.getParkedVehicles();
		
		return ResponseEntity.ok(parkedVehicles);
	}
	

	@GetMapping("/free-slots")
	public ResponseEntity<Object> getListOfFreeParkingSlot() {
		
		List<ParkingLotDTO> freeParkingSlot = parkingService.getFreeParkingSlot();
		
		return ResponseEntity.ok(freeParkingSlot);
	}
	
	@GetMapping("/parked/vehicle/{number}")
	public ResponseEntity<Object> getParkedVehicle(@PathVariable("number") String number) {
		
		ParkedVehiclesDTO parkedVehicle  = parkingService.getParkedVehicle(number);
		
		return ResponseEntity.ok(parkedVehicle);
	}
	
	@PostMapping("/park-in")
	public ResponseEntity<Object> parkReturningVehicle(@RequestBody ParkVehicleDTO parkVehicle) {
		
		parkingService.parkVehicle(parkVehicle);
		
		SuccessResponseDTO success = new SuccessResponseDTO(200, 
				String.format("Vehicle %s was successfully parked.", parkVehicle.getVehicleNumber()));
		
		return ResponseEntity.ok(success);
	}
	
	@PostMapping("/park-in-new")
	public ResponseEntity<Object> parkNewVehicle(@RequestBody ParkVehicleDTO parkVehicle) {
		
		parkingService.parkVehicleNew(parkVehicle);
		
		SuccessResponseDTO success = new SuccessResponseDTO(200, 
				String.format("Vehicle %s was successfully added and parked.", parkVehicle.getVehicleNumber()));
		
		return ResponseEntity.ok(success);
	}
	
	@PutMapping("/park-out/{vehicleNumber}")
	public ResponseEntity<Object> parkOutVehicle(@PathVariable("vehicleNumber") String vehicleNumber) {
		
		ParkedVehiclesDTO parkedVehicle = parkingService.parkOut(vehicleNumber);
		
		return ResponseEntity.ok(parkedVehicle);
	}
	
	
}
