# Maven
# mvn clean package -Dmaven.test.skip=true
# Docker
# docker build -t parking-api .


FROM openjdk:11-jdk

ADD target/parking-system-0.0.1-SNAPSHOT.jar app.jar

EXPOSE 8080

ENTRYPOINT ["java", "-jar", "-Dspring.profiles.active=docker", "app.jar"]
